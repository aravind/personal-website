function App() {
  return (
    <>
      <div className="flex min-h-screen flex-col pt-6">
        <div className="flex justify-start justify-items-start">
          <h1 className="inline-block bg-gradient-to-r from-blue-silica to-green-silica bg-clip-text text-5xl leading-tight text-transparent">
            Aravind Nidadavolu
          </h1>
        </div>
        <div className="justify-start">
          <p className="inline-flex font-hack text-xl text-gray-400">
            <span className="inline pr-2 text-cyan-silica">❯ </span>
            <span className="typing-effect inline-block">
              Senior Software Engineer
            </span>
          </p>
        </div>
        <hr className="my-4 border-gray-800" />
        <div className="flex justify-start">
          <p className="pt-3">This site is currently under construction</p>
        </div>
      </div>
    </>
  );
}

export default App;
