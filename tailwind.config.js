/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          "Inter",
          "system-ui",
          "Avenir",
          "Helvetica",
          "Arial",
          "sans-serif",
        ],
        hack: ["Hack", "monospace"],
      },
      colors: {
        "bg-sillica": "#0e1419",
        "fg-silica": "#e5e1cf",
        "black-silica": "#000000",
        "red-silica": "#ff3333",
        "green-silica": "#83b152",
        "yellow-silica": "#e6c446",
        "blue-silica": "#3695d9",
        "purple-silica": "#d25091",
        "cyan-silica": "#2de1b4",
        "white-silica": "#c5c5c5",
      },
    },
  },
  plugins: [],
};
